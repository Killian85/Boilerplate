/* ==========================================================================
  Elements (p, a, h1, em...)
   ========================================================================== */

/*
Les éléments (namespace:) e- sont les éléments HTML natifs que nous ne stylerions pas en fonction des noms de classe
*/


/* ==========================================================================
    Patterns (Header, Sidebar, Section...)
   ========================================================================== */


/* ==========================================================================
   Scope
   ========================================================================== */

/*
  Utiliser la portée seulement si elle est absolument nécessaire. 
  Le but de la portée (namespace:) s- est de nous donner la plus grande spécificité afin que nous puissions écraser tous les styles dans un but spécifique.
*/


/* ==========================================================================
   Utility
   ========================================================================== */

/*
 apporter des modifications uniquement à un style particulier à un endroit spécifique. 
 Dans ce cas, les classes utilitaires (namespace :) u- peuvent nous aider à le mettre à jour sans modifier la structure CSS dans son ensemble
*/